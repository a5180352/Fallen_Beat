package Control;

import Visual.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;

public class Frame extends JFrame implements ActionListener {

    PanelMenu pMenu = new PanelMenu();
    PanelHelp pHelp = new PanelHelp();
    PanelHell pHell = new PanelHell();
    PanelHighScore pHighScore = new PanelHighScore();
    PanelStrat pStart = new PanelStrat();
    PanelGaming pGaming;
    PanelEnding pEnding = new PanelEnding();
    GameControl gc;

    AudioPlayer apImSoHappy = new AudioPlayer("songs/I'm so Happy/I'm so Happy.wav");
    AudioPlayer apGekkou = new AudioPlayer("songs/gekkou/gekkou.wav");
    AudioPlayer titleBGM = new AudioPlayer("songs/titleBGM.wav");

    boolean trialL = false;
    boolean trialR = false;

    public Frame(String s) {
        super(s);

        pMenu.btnStart.addActionListener(this);
        pMenu.btnHighScore.addActionListener(this);
        pMenu.btnHelp.addActionListener(this);
        pMenu.btnExit.addActionListener(this);
        add(pMenu);

        pHelp.btnBack.addActionListener(this);
        pHelp.btnHell.addActionListener(this);

        pHighScore.btnBack.addActionListener(this);

        pHell.btnBack.addActionListener(this);
        pHell.btnHell.addActionListener(this);

        pStart.btnBack.addActionListener(this);
        for (int i = 0; i < 3; ++i) {
            pStart.btnL[i].addActionListener(this);
            pStart.btnR[i].addActionListener(this);
        }

        pEnding.btnBack.addActionListener(this);
        titleBGM.musicPlay();
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        // Menu buttons
        if (ae.getSource() == pMenu.btnStart) {
            if (titleBGM.clip != null && titleBGM.clip.isRunning()) {
                titleBGM.musicStop();
            }
            remove(pMenu);
            add(pStart);
            repaint();
            revalidate();
        } else if (ae.getSource() == pMenu.btnHighScore) {
            remove(pMenu);
            add(pHighScore);
            try {
                pHighScore.readRecord();
            } catch (Exception ex) {
                System.out.println(ex);
            }
            repaint();
            revalidate();
        } else if (ae.getSource() == pMenu.btnHelp) {
            remove(pMenu);
            add(pHelp);
            repaint();
            revalidate();
        } else if (ae.getSource() == pMenu.btnExit) {
            if (titleBGM.clip != null && titleBGM.clip.isRunning()) {
                titleBGM.musicStop();
            }
            dispose();
        }

        // Help button
        if (ae.getSource() == pHelp.btnBack) {
            remove(pHelp);
            add(pMenu);
            repaint();
            revalidate();
        } else if (ae.getSource() == pHelp.btnHell) {
            if (titleBGM.clip != null && titleBGM.clip.isRunning()) {
                titleBGM.musicStop();
            }
            remove(pHelp);
            add(pHell);
            try {
                pHell.readRecord();
            } catch (Exception ex) {
                System.out.println(ex);
            }
            repaint();
            revalidate();
        }

        // Hell button
        if (ae.getSource() == pHell.btnBack) {
            remove(pHell);
            add(pMenu);
            titleBGM.musicPlay();
            repaint();
            revalidate();
        } else if (ae.getSource() == pHell.btnHell) {
            pGaming = new PanelGaming();
            remove(pHell);
            add(pGaming);
            repaint();
            revalidate();

            gc = new GameControl(this, pEnding, pGaming, "songs/gekkou/hell.txt");
            gc.setInfo(1, 2);
            gc.setMusic("songs/gekkou/gekkou_hell.wav");
            gc.setBanner("songs/gekkou/banner.png");
            gc.setDelay(560, -5);
            gc.start();
        }

        // HighScore buttons
        if (ae.getSource() == pHighScore.btnBack) {
            remove(pHighScore);
            add(pMenu);
            repaint();
            revalidate();
        }

        // Start Buttons
        if (ae.getSource() == pStart.btnBack) {
            if (trialL) {
                apImSoHappy.musicStop();
                trialL = false;
                pStart.btnL[0].setText("Trial");
            }
            if (trialR) {
                apGekkou.musicStop();
                trialR = false;
                pStart.btnR[0].setText("Traii");
            }
            remove(pStart);
            add(pMenu);
            titleBGM.musicPlay();
            repaint();
            revalidate();
        }

        if (ae.getSource() == pStart.btnL[0]) {
            if (trialR) {
                apGekkou.musicStop();
                trialR = false;
                pStart.btnR[0].setText("Trial");
            }

            if (!trialL) {
                apImSoHappy.musicPlay();
                trialL = true;
                pStart.btnL[0].setText("Stop");
            } else {
                apImSoHappy.musicStop();
                trialL = false;
                pStart.btnL[0].setText("Trial");
            }
        } else if (ae.getSource() == pStart.btnL[1]) {
            if (trialL) {
                apImSoHappy.musicStop();
                trialL = false;
                pStart.btnL[0].setText("Triai");
            }
            if (trialR) {
                apGekkou.musicStop();
                trialR = false;
                pStart.btnR[0].setText("Trial");
            }

            pGaming = new PanelGaming();
            remove(pStart);
            add(pGaming);
            repaint();
            revalidate();

            gc = new GameControl(this, pEnding, pGaming, "songs/I'm so Happy/easy.txt");
            gc.setInfo(0, 0);
            gc.setMusic("songs/I'm so Happy/I'm so Happy.wav");
            gc.setBanner("songs/I'm so Happy/banner.png");
            gc.setDelay(-450, -102);
            gc.start();
        } else if (ae.getSource() == pStart.btnL[2]) {
            if (trialL) {
                apImSoHappy.musicStop();
                trialL = false;
                pStart.btnL[0].setText("Trial");
            }
            if (trialR) {
                apGekkou.musicStop();
                trialR = false;
                pStart.btnR[0].setText("Trial");
            }

            pGaming = new PanelGaming();
            remove(pStart);
            add(pGaming);
            repaint();
            revalidate();

            gc = new GameControl(this, pEnding, pGaming, "songs/I'm so Happy/hard.txt");
            gc.setInfo(0, 1);
            gc.setMusic("songs/I'm so Happy/I'm so Happy.wav");
            gc.setBanner("songs/I'm so Happy/banner.png");
            gc.setDelay(-450, -102);
            gc.start();
        }

        if (ae.getSource() == pStart.btnR[0]) {
            if (trialL) {
                apImSoHappy.musicStop();
                trialL = false;
                pStart.btnL[0].setText("Trial");
            }

            if (!trialR) {
                apGekkou.musicPlay();
                trialR = true;
                pStart.btnR[0].setText("Stop");
            } else {
                apGekkou.musicStop();
                trialR = false;
                pStart.btnR[0].setText("Trial");
            }
        } else if (ae.getSource() == pStart.btnR[1]) {
            if (trialL) {
                apImSoHappy.musicStop();
                trialL = false;
                pStart.btnL[0].setText("Trial");
            }
            if (trialR) {
                apGekkou.musicStop();
                trialR = false;
                pStart.btnR[0].setText("Trial");
            }

            pGaming = new PanelGaming();
            remove(pStart);
            add(pGaming);
            repaint();
            revalidate();

            gc = new GameControl(this, pEnding, pGaming, "songs/gekkou/easy.txt");
            gc.setInfo(1, 0);
            gc.setMusic("songs/gekkou/gekkou.wav");
            gc.setBanner("songs/gekkou/banner.png");
            gc.setDelay(740, 11);
            gc.start();
        } else if (ae.getSource() == pStart.btnR[2]) {
            if (trialL) {
                apImSoHappy.musicStop();
                trialL = false;
                pStart.btnL[0].setText("Trial");
            }
            if (trialR) {
                apGekkou.musicStop();
                trialR = false;
                pStart.btnR[0].setText("Trial");
            }

            pGaming = new PanelGaming();
            remove(pStart);
            add(pGaming);
            repaint();
            revalidate();

            gc = new GameControl(this, pEnding, pGaming, "songs/gekkou/hard.txt");
            gc.setInfo(1, 1);
            gc.setMusic("songs/gekkou/gekkou.wav");
            gc.setBanner("songs/gekkou/banner.png");
            gc.setDelay(740, 11);
            gc.start();
        }

        // Ending Button
        if (ae.getSource() == pEnding.btnBack) {
            remove(pEnding);
            pEnding.close();
            add(pStart);
            repaint();
            revalidate();
        }
    }
}
